package gamedata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import template.BuzzwordTemplate;

import java.io.File;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static gamedata.ModeState.*;

/**
 * Created by Yuege on 11/8/2016.
 */
public class BuzzwordData {
    private String username;
    private String password;
    private int[] userProgress=new int[4];
    private int[][] personalBest=new int[4][5];
    private ObjectMapper mapper;
    private Map<String,Object> dataMap;
    private MessageDigest md5;
    private final String[] MODES={"SCIENCE","ANIMALS","PLACES","FOOD"};

    public BuzzwordData() {
        try {
            for(int i=0;i<4;i++) {
                userProgress[i]=1;
                for (int j = 0; j < 5; j++)
                    personalBest[i][j] = 0;
            }
            md5=MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public boolean createNewProfile(String username,String password){
        try{
            URL         workDirURL  = BuzzwordTemplate.class.getClassLoader().getResource("userFile");
            File userFile=new File(workDirURL.getFile());
            userFile=new File(userFile.toString()+File.separator+username+".json");
            if (!userFile.exists()) {
                userFile.createNewFile();
            }
            else {
                return false;
            }
            this.username=username;
            dataMap = new HashMap<String, Object>();
            mapper = new ObjectMapper();
            userProgress= new int[4];
            personalBest=new int[4][5];

            md5.update(password.getBytes());
            this.password= Base64.encode(md5.digest());

            dataMap.put("username",  this.username);
            dataMap.put("password",  this.password);
            for(int i=0;i<4;i++) {
                userProgress[i]=1;
                for (int j = 0; j < 5; j++)
                    personalBest[i][j] = 0;
            }
            dataMap.put("personalBest", personalBest);
            dataMap.put("userProgress", userProgress);
            mapper.writeValue(userFile, dataMap);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    public boolean logIn(String username,String password){
        try{
            URL         workDirURL  = BuzzwordTemplate.class.getClassLoader().getResource("userFile/"+username+".json");
            if(workDirURL==null)
                return false;
            File   userFile=new File(workDirURL.getFile());
            if(!userFile.exists())
                return false;
            dataMap = new HashMap<String, Object>();
            mapper = new ObjectMapper();
            personalBest= new int[4][5];
            userProgress= new int[4];
            md5.update(password.getBytes());
            this.password= Base64.encode(md5.digest());

            dataMap = mapper.readValue(userFile, Map.class);
            String s= (String) dataMap.get("password");
            if(!this.password.equals(s)){
                return false;
            }
            this.username= (String) dataMap.get("username");
            ArrayList best= (ArrayList) dataMap.get("personalBest");
            ArrayList progress=(ArrayList)dataMap.get("userProgress");
            for(int i=0;i<4;i++) {
                userProgress[i]= (int) progress.get(i);
                for (int j = 0; j < 5; j++) {
                    personalBest[i][j]= (int) ((ArrayList) (best.get(i))).get(j);
                }
            }
        }
        catch (Exception e){e.printStackTrace();}
        return true;
    }
    public void saveUserProgress(String mode,String level,int newRecord){
        dataMap = new HashMap<String, Object>();
        mapper = new ObjectMapper();
        int modeInt;
        try{
            switch (mode){
                case "Places":modeInt=PLACES.getParameter();break;
                case "Science": modeInt=SCIENCE.getParameter();break;
                case "Animals": modeInt=ANIMALS.getParameter();break;
                default: modeInt=FOOD.getParameter();break;
            }
            if(userProgress[modeInt]==5||userProgress[modeInt]>Integer.parseInt(level))
                return;
            else
                userProgress[modeInt]++;
            int l=Integer.parseInt(level)-1;
            personalBest[modeInt][l]=newRecord;
            URL         workDirURL  = BuzzwordTemplate.class.getClassLoader().getResource("userFile");
            File userFile=new File(workDirURL.getFile());
            userFile=new File(userFile.toString()+File.separator+username+".json");

            dataMap.put("username",  this.username);
            dataMap.put("password",  this.password);
            dataMap.put("personalBest", personalBest);
            dataMap.put("userProgress", userProgress);
            mapper.writeValue(userFile, dataMap);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    public boolean saveUserProgress(String oldName,boolean rename){
        dataMap = new HashMap<String, Object>();
        mapper = new ObjectMapper();

        try{

            URL         workDirURL  = BuzzwordTemplate.class.getClassLoader().getResource("userFile");
            File userFileDir=new File(workDirURL.getFile());
            File userFile=new File(userFileDir.toString()+File.separator+oldName+".json");
            if(rename) {
                if (new File(userFileDir.toString() + File.separator + username + ".json").exists())
                    return false;
            }
            dataMap.put("username",  this.username);
            dataMap.put("password",  this.password);
            dataMap.put("personalBest", personalBest);
            dataMap.put("userProgress", userProgress);
            mapper.writeValue(userFile, dataMap);
            if(rename) {
                return userFile.renameTo(new File(userFileDir.toString() + File.separator + username + ".json"));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }
    public String getUsername(){
        return username;
    }
    public int[] getUserProgress(){return userProgress;}

    public boolean setUsername(String username){
        String oldName=this.username;
        this.username=username;
        return saveUserProgress(oldName,true);
    }

    public boolean setPassword(String oldPassword,String newPassword){
        md5.update(oldPassword.getBytes());
        oldPassword= Base64.encode(md5.digest());
        if(oldPassword.equals(password)){
            md5.update(newPassword.getBytes());
            password=Base64.encode(md5.digest());
            return saveUserProgress(username,false);
        }
        else
            return false;
    }
    public String getUserInformation(){
        String[] personBest={"","","",""};
        for (int i=0;i<4;i++){
            for(int j:personalBest[i]) {
                if (j == 0)
                    personBest[i] = personBest[i] + "NA ";
                else
                    personBest[i] = personBest[i] + j + "s ";
            }
        }
        return("Username: "+username+"\r\nUser progress in each mode:\r\n" +
                "Places: "+userProgress[0]+"\r\n" +
                "Science: "+userProgress[1]+"\r\n" +
                "Animals: "+userProgress[2]+"\r\n" +
                "Food: "+userProgress[3]+"\r\n" +
                "User best records in each level of each mode:\r\n" +
                "Places: "+personBest[0]+"\r\n" +
                "Science: "+personBest[1]+"\r\n" +
                "Animals: "+personBest[2]+"\r\n" +
                "Food: "+personBest[3]);
    }
    public int getRecord(String mode,String level){
        int modeInt,l=Integer.parseInt(level)-1;
        switch (mode){
            case "Places":modeInt=PLACES.getParameter();break;
            case "Science": modeInt=SCIENCE.getParameter();break;
            case "Animals": modeInt=ANIMALS.getParameter();break;
            default: modeInt=FOOD.getParameter();break;
        }
        return personalBest[modeInt][l];
    }
}
