package gamedata;

/**
 * Created by Yuege on 11/26/2016.
 */
public enum ModeState {
    PLACES(0),
    SCIENCE(1),
    ANIMALS(2),
    FOOD(3);


    private int parameter;

    ModeState(int parameter) {
        this.parameter = parameter;
    }

    public int getParameter() {
        return parameter;
    }
}