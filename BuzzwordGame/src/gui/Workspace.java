package gui;

import gamedata.BuzzwordData;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import template.BuzzwordTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;

import static gamedata.ModeState.*;

/**
 * Created by Yuege on 11/8/2016.
 */
public class Workspace {
    private BuzzwordTemplate buzzwordTemplate;
    private BuzzwordData gameData;
    ArrayList<String> a34;
    ArrayList<String> a56;
    ArrayList<String> a78;
    ArrayList<String> a910;
    ArrayList<String> af;
    static final ArrayList<ArrayList> adjencyList = AdjecncyList.getAdjencyList();
    private String currentGuess="";
    private Set<String> wordKey=new HashSet<>();
    private ArrayList<ArrayList> lastPaths=new ArrayList<>();

    public Workspace(BuzzwordTemplate b,BuzzwordData d){
        buzzwordTemplate=b;
        gameData=d;
    }
    public void renderUserInformation(){
        String name=gameData.getUsername();
        buzzwordTemplate.getGui().getHomeScreen().getProfileButton().setText(name);
        buzzwordTemplate.getGui().getLevelSelectionScreen().getProfileButton().setText(name);
        buzzwordTemplate.getGui().getGamePlayScreen().getProfileButton().setText(name);
    }
    public void renderLevelSelection(){
        int level;
        int[] progress=gameData.getUserProgress();
        String mode=buzzwordTemplate.getGui().getHomeScreen().getModeString();
        GridPane p=buzzwordTemplate.getGui().getLevelSelectionScreen().getLevelNodes();
        switch (mode){
            case "Places":level=PLACES.getParameter();break;
            case "Science": level=SCIENCE.getParameter();break;
            case "Animals": level=ANIMALS.getParameter();break;
            default: level=FOOD.getParameter();break;
        }
        level=progress[level];
        for(int i=0;i<level;i++)
            p.getChildren().get(i).setDisable(false);
        for (int i=level;i<5;i++)
            p.getChildren().get(i).setDisable(true);

    }
    public void readWord(String mode){
        try {

            URL imgDirURL = getClass().getClassLoader().getResource("wordlist" + File.separator + mode + ".txt");
            File file = new File(imgDirURL.toURI());
            InputStreamReader read = new InputStreamReader(new FileInputStream(file));
            BufferedReader bufferedReader = new BufferedReader(read);
            String lineTxt;
            ArrayList<String> a = new ArrayList<String>();
            while ((lineTxt = bufferedReader.readLine()) != null) {
                String[] s1 = lineTxt.split(" ");
                for (String s : s1)
                    a.add(s);
            }
            read.close();
            a34 = new ArrayList<String>();
            a56 = new ArrayList<String>();
            a78 = new ArrayList<String>();
            a910 = new ArrayList<String>();
            af = new ArrayList<String>();
            for (String s : a) {
                if (s.length() >= 3 && s.length() <= 4)
                    a34.add(s);
                else if (s.length() >= 5 && s.length() <= 6)
                    a56.add(s);
                else if (s.length() >= 7 && s.length() <= 8)
                    a78.add(s);
                else if (s.length() >= 9 && s.length() <= 10)
                    a910.add(s);
                else if (s.length() > 10)
                    af.add(s);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    public void renderGamePlayScreen(String level){
       while (true){
           if (generateWords(level))
               break;
       };
    }
    public boolean generateWords(String level){
        try{
            wordKey=new HashSet<>();
            int lev=Integer.parseInt(level);
            ArrayList used=new ArrayList();
            int first,index;
            GridPane p=buzzwordTemplate.getGui().getGamePlayScreen().getGameDataPane();
            ArrayList guess,guessedWords=new ArrayList();
            switch (lev){
                case 1:
                    guess=new ArrayList(12);
                    index=(int)(Math.random()*16);
                    used.add(index);
                    while (guess.size()!=12) {
                        first=(int) (Math.random()*a34.size());
                        if(guessedWords.contains(a34.get(first))||(a34.get(first).length()!=3))
                            continue;
                        else
                            guessedWords.add(a34.get(first));
                        for (char c : a34.get(first).toCharArray()) {
                            if (guess.size()!=12) {
                                guess.add(c);
                                buzzwordTemplate.getGui().getGamePlayScreen().addText((c + "").toLowerCase(), (HBox) p.getChildren().get(index));
                                boolean failed=true;
                                for(Object q:adjencyList.get(index+1)){
                                    if(!used.contains((int)q-1))
                                        failed=false;
                                }
                                if(failed) {
                                    buzzwordTemplate.getGui().getGamePlayScreen().clean();
                                    return false;
                                }
                                while (true) {
                                    int i = (int) (Math.random() * adjencyList.get(index+1).size());

                                    int j= (int) adjencyList.get(index+1).get(i)-1;

                                    if(used.contains(j))
                                        continue;
                                    else {
                                        index=j;
                                        used.add(index);
                                        break;
                                    }
                                }
                            }
                        }
                        if(!wordKey.contains(a34.get(first)))
                            wordKey.add(a34.get(first));
                        if(guess.size()==12) {
                            for(String s:a34)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a56)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a78)
                                if(isSolution(s))
                                    wordKey.add(s);
                            break;
                        }
                    }
                    break;
                case 2:
                    guess=new ArrayList<>(14);
                    index=(int)(Math.random()*16);
                    used.add(index);
                    int words3=0,words4=0;
                    while (guess.size()!=14) {
                        first=(int) (Math.random()*a34.size());
                        if(guessedWords.contains(a34.get(first)))
                            continue;
                        else if(a34.get(first).length()==4&&words4==2)
                            continue;
                        else if(a34.get(first).length()==3&&words3==2)
                            continue;
                        else if(a34.get(first).length()==3) {
                            guessedWords.add(a34.get(first));
                            words3++;
                        }
                        else if(a34.get(first).length()==4){
                            guessedWords.add(a34.get(first));
                            words4++;
                        }
                        for (char c : a34.get(first).toCharArray()) {
                            if (guess.size()!=14) {
                                guess.add(c);
                                buzzwordTemplate.getGui().getGamePlayScreen().addText((c + "").toLowerCase(), (HBox) p.getChildren().get(index));
                                boolean failed=true;
                                for(Object q:adjencyList.get(index+1)){
                                    if(!used.contains((int)q-1))
                                        failed=false;
                                }
                                if(failed) {
                                    buzzwordTemplate.getGui().getGamePlayScreen().clean();
                                    return false;
                                }
                                while (true) {
                                    int i = (int) (Math.random() * adjencyList.get(index+1).size());

                                    int j= (int) adjencyList.get(index+1).get(i)-1;

                                    if(used.contains(j))
                                        continue;
                                    else {
                                        index=j;
                                        used.add(index);
                                        break;
                                    }
                                }
                            }
                        }
                        if(!wordKey.contains(a34.get(first)))
                            wordKey.add(a34.get(first));
                        if(guess.size()==14){
                            for(String s:a34)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a56)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a78)
                                if(isSolution(s))
                                    wordKey.add(s);
                            break;
                        }

                    }
                    break;
                case 3:
                    guess=new ArrayList(15);
                    index=(int)(Math.random()*16);
                    used.add(index);
                    while (guess.size()!=15) {
                        first=(int) (Math.random()*a56.size());
                        if(guessedWords.contains(a56.get(first))||(a56.get(first).length()!=5))
                            continue;
                        else
                            guessedWords.add(a56.get(first));
                        for (char c : a56.get(first).toCharArray()) {
                            if (guess.size()!=15) {
                                guess.add(c);
                                buzzwordTemplate.getGui().getGamePlayScreen().addText((c + "").toLowerCase(), (HBox) p.getChildren().get(index));
                                boolean failed=true;
                                for(Object q:adjencyList.get(index+1)){
                                    if(!used.contains((int)q-1))
                                        failed=false;
                                }
                                if(failed) {
                                    buzzwordTemplate.getGui().getGamePlayScreen().clean();
                                    return false;
                                }
                                while (true) {
                                    int i = (int) (Math.random() * adjencyList.get(index+1).size());

                                    int j= (int) adjencyList.get(index+1).get(i)-1;

                                    if(used.contains(j))
                                        continue;
                                    else {
                                        index=j;
                                        used.add(index);
                                        break;
                                    }
                                }
                            }
                        }
                        if(!wordKey.contains(a56.get(first)))
                            wordKey.add(a56.get(first));
                        if(guess.size()==15){
                            for(String s:a34)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a56)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a78)
                                if(isSolution(s))
                                    wordKey.add(s);
                            break;
                        }
                    }
                    break;
                case 4:
                    guess=new ArrayList(15);
                    index=(int)(Math.random()*16);
                    used.add(index);
                    int words6=0;
                    while (guess.size()!=15) {
                        first=(int) (Math.random()*a56.size());
                        String s3;
                        do{int second=(int) (Math.random()*a34.size());
                            s3=a34.get(second);}
                        while(s3.length()!=3);
                        if(guessedWords.contains(a56.get(first)))
                            continue;
                        else if(a56.get(first).length()!=6)
                            continue;
                        else if(a56.get(first).length()==6) {
                            guessedWords.add(a56.get(first));
                            words6++;
                        }
                        String s56=a56.get(first);
                        if(words6>=3)
                            s56=s3;
                        for (char c : s56.toCharArray()) {
                            if (guess.size()!=15) {
                                guess.add(c);
                                buzzwordTemplate.getGui().getGamePlayScreen().addText((c + "").toLowerCase(), (HBox) p.getChildren().get(index));
                                boolean failed=true;
                                for(Object q:adjencyList.get(index+1)){
                                    if(!used.contains((int)q-1))
                                        failed=false;
                                }
                                if(failed) {
                                    buzzwordTemplate.getGui().getGamePlayScreen().clean();
                                    return false;
                                }
                                while (true) {
                                    int i = (int) (Math.random() * adjencyList.get(index+1).size());

                                    int j= (int) adjencyList.get(index+1).get(i)-1;

                                    if(used.contains(j))
                                        continue;
                                    else {
                                        index=j;
                                        used.add(index);
                                        break;
                                    }
                                }
                            }
                        }
                        if(!wordKey.contains(s56))
                            wordKey.add(s56);
                        if(guess.size()==15){
                            for(String s:a34)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a56)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a78)
                                if(isSolution(s))
                                    wordKey.add(s);
                            int totalScore=0;
                            for(String s:wordKey){
                                totalScore+=contain(s);
                                if(totalScore>=175)
                                    break;
                            }
                            if(totalScore<175)
                                return false;
                            break;
                        }

                    }
                    break;
                default:
                    guess=new ArrayList(15);
                    index=(int)(Math.random()*16);
                    used.add(index);
                    String s7=null,s8=null;boolean b7=false;
                    while (guess.size()!=15) {
                        while (s7==null||s8==null) {
                            first = (int) (Math.random() * a78.size());
                            if ((a78.get(first).length() == 7))
                                s7 = a78.get(first);
                            else
                                s8 = a78.get(first);
                        }
                        if(b7)
                            s7=s8;
                        for (char c : s7.toCharArray()) {
                            if (guess.size()!=15) {
                                guess.add(c);
                                buzzwordTemplate.getGui().getGamePlayScreen().addText((c + "").toLowerCase(), (HBox) p.getChildren().get(index));
                                boolean failed=true;
                                for(Object q:adjencyList.get(index+1)){
                                    if(!used.contains((int)q-1))
                                        failed=false;
                                }
                                if(failed) {
                                    buzzwordTemplate.getGui().getGamePlayScreen().clean();
                                    return false;
                                }
                                while (true) {
                                    int i = (int) (Math.random() * adjencyList.get(index+1).size());

                                    int j= (int) adjencyList.get(index+1).get(i)-1;

                                    if(used.contains(j))
                                        continue;
                                    else {
                                        index=j;
                                        used.add(index);
                                        break;
                                    }
                                }
                            }
                        }
                        b7=true;
                        if(!wordKey.contains(s7))
                            wordKey.add(s7);
                        if(guess.size()==15){
                            for(String s:a34)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a56)
                                if(isSolution(s))
                                    wordKey.add(s);
                            for(String s:a78)
                                if(isSolution(s))
                                    wordKey.add(s);
                            int totalScore=0;
                            for(String s:wordKey){
                                totalScore+=contain(s);
                                if(totalScore>=200)
                                    break;
                            }
                            if(totalScore<200)
                                return false;
                            break;
                        }
                    }
                    break;
            }
        }
        catch (Exception e){e.printStackTrace();}
        return true;
    }

    private boolean isSolution(String s) {
        ArrayList<ArrayList> lastPaths=new ArrayList<>();
        for (char c:s.toCharArray()) {
            if(!isSolution(c, lastPaths))
                return false;
        }
        return true;
    }
    private boolean isSolution(char c,ArrayList<ArrayList> lastPaths){
        GridPane p=buzzwordTemplate.getGui().getGamePlayScreen().getGameDataPane();
        char guess=c;
        boolean noGood=true;
        ArrayList newPath;
        if(lastPaths.isEmpty()){
            for(int j=0;j<16;j++){
                HBox hBox= (HBox) p.getChildren().get(j);
                if(!((Label)hBox.getChildren().get(0)).getText().isEmpty()) {
                    if(((Label)hBox.getChildren().get(0)).getText().charAt(0)==guess) {
                        newPath=new ArrayList();
                        newPath.add(j);
                        lastPaths.add(newPath);
                        noGood = false;
                    }
                }
            }
            if(noGood)
                return false;
        }
        else {
            ArrayList<ArrayList> lastPositions = new ArrayList();
            for (ArrayList path : lastPaths) {
                lastPositions.add(path);
            }
            lastPaths.clear();
            for (ArrayList o : lastPositions) {
                int lastPo = (Integer) o.get(o.size() - 1);
                for (Object q : adjencyList.get(lastPo+1)) {
                    HBox hBox = (HBox) p.getChildren().get((int) q - 1);
                    if (!((Label) hBox.getChildren().get(0)).getText().isEmpty()) {
                        if (((Label) hBox.getChildren().get(0)).getText().charAt(0) == guess&&!o.contains((int)q-1)) {
                            ArrayList newP=new ArrayList();
                            for (Object t:o){
                                newP.add(t);
                            }
                            newP.add((int) q - 1);
                            lastPaths.add(newP);
                        }
                    }
                }
            }
            if (lastPaths.isEmpty() ) {
                return false;
            }
        }
        return true;

    }

    public void drawLines(GraphicsContext gc){
        gc.setLineWidth(4);
        gc.setStroke(Color.BLACK);
        GridPane p=buzzwordTemplate.getGui().getGamePlayScreen().getGameDataPane();
        double w40=((HBox)p.getChildren().get(0)).getMaxHeight()/2;
        double x130=p.getHgap()+w40*2;
        for(int i=1;i<=4;i++){
            gc.strokeLine(w40,w40+(i-1)*x130, w40+x130*3,w40+(i-1)*x130);
            gc.strokeLine(w40+(i-1)*x130,w40,w40+(i-1)*x130, w40+x130*3);
        }
        for(int i=1;i<=5;i++){
            if(i<=3) {
                gc.strokeLine(w40, w40+x130 + (i - 1) * x130, w40+x130 + x130 * (i - 1), w40);
                gc.strokeLine(w40, w40+x130*2 - (i - 1) * x130, w40+x130 + (i - 1) * x130, w40+x130*3);
            }
            else {
                gc.strokeLine(w40+x130 + x130 * (i - 4),  w40+x130*3,  w40+x130*3, w40+x130 + x130 * (i - 4));
                gc.strokeLine(w40+x130+(i-4)*x130,w40, w40+x130*3,w40+x130*2-(i-4)*x130);
            }
        }
    }
    public void renderPlayGui(boolean play) {

        Canvas canvas=buzzwordTemplate.getGui().getGamePlayScreen().getCanvas();
        GraphicsContext gc=canvas.getGraphicsContext2D();
        drawLines(gc);
        HBox[] buzznodes=new HBox[16];
        GridPane p=buzzwordTemplate.getGui().getGamePlayScreen().getGameDataPane();
        for(int i=0;i<16;i++) {
            buzznodes[i] = (HBox) p.getChildren().get(i);
        }
        buzzwordTemplate.getGui().getGamePlayScreen().getGameScene().setOnDragOver(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                /* data is dragged over the target */
                /* accept it only if it is  not dragged from the same node
                 * and if it has a string data */
                event.acceptTransferModes(TransferMode.ANY);
                double x=event.getX();
                double y=event.getY();
                gc.setFill(Color.GOLD);
                Bounds boundsInScene = p.localToScene(p.getBoundsInLocal());
                gc.fillOval(x-boundsInScene.getMinX()-7,y-boundsInScene.getMinY()-5,12,12);
                event.consume();
            }
        });
        if(!lastPaths.isEmpty()){
            for(ArrayList a:lastPaths){
                highLightPaths(a,gc,Color.GOLD);
            }
        }
        ArrayList lastDrag = new ArrayList();
        int i=-1;
        for(HBox hBox:buzznodes) {
            i++;
            if(!play){
                hBox.setDisable(true);
                continue;
            }
            if(((Label)hBox.getChildren().get(0)).getText().isEmpty()){
                hBox.setDisable(true);
                continue;
            }
            hBox.setDisable(false);
            int finalI = i;
            hBox.setOnDragDetected(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent event) {
                    hBox.setScaleX(1.2);hBox.setScaleY(1.2);
                    hBox.getChildren().get(0).setStyle("-fx-text-fill: Gold");
                    Dragboard db = hBox.startDragAndDrop(TransferMode.ANY);
                    ClipboardContent content = new ClipboardContent();
                    content.putString("test");
                    db.setContent(content);
                    event.consume();
                }
            });

            hBox.setOnDragEntered(new EventHandler<DragEvent>() {
                public void handle(DragEvent event) {
                    gc.clearRect(0,0,canvas.getWidth(),canvas.getHeight());
                    drawLines(gc);
                    if((lastDrag.contains(finalI))||(!lastDrag.isEmpty()&&!adjencyList.get((Integer) lastDrag.get(lastDrag.size()-1)+1).contains(finalI+1))) {
                        lastDrag.clear();
                        gc.clearRect(0,0,canvas.getWidth(),canvas.getHeight());
                        drawLines(gc);
                        currentGuess="";
                        buzzwordTemplate.getGui().getGamePlayScreen().setCurentGuessBox(currentGuess);
                        for(HBox h:buzznodes){
                            h.setScaleX(1);h.setScaleY(1);
                            h.getChildren().get(0).setStyle("-fx-text-fill: Black");
                        }
                        return;
                    }
                    else{
                        lastDrag.add(finalI);
                        lastPaths.add(lastDrag);
                    }
                    for(Object i:lastDrag)
                        if((int)i!=finalI)
                            highLightLine((int)i+1, 1+(Integer) lastDrag.get(lastDrag.indexOf(i)+1),gc);
                    currentGuess=currentGuess+ ((Label)hBox.getChildren().get(0)).getText();
                    buzzwordTemplate.getGui().getGamePlayScreen().setCurentGuessBox(currentGuess);
                    hBox.setScaleX(1.2);hBox.setScaleY(1.2);
                    hBox.getChildren().get(0).setStyle("-fx-text-fill: Gold");
                    event.consume();
                }
            });

            hBox.setOnDragExited(new EventHandler<DragEvent>() {
                public void handle(DragEvent event) {
                    hBox.setScaleX(1.2);hBox.setScaleY(1.2);
                    hBox.getChildren().get(0).setStyle("-fx-text-fill: Gold");
                    event.consume();
                }
            });
            hBox.setOnDragOver(event1 -> {
                event1.acceptTransferModes(TransferMode.ANY);
                event1.consume();
            });
            //Enable a label to be dropped on this label
            hBox.setOnDragDropped(new EventHandler<DragEvent>() {
                public void handle(DragEvent event) {
                    event.consume();
                }
            });
            hBox.setOnDragDone(new EventHandler<DragEvent>() {
                public void handle(DragEvent event) {
                    lastDrag.clear();
                    lastPaths.clear();
                    int i=contain(currentGuess);
                    if(i!=0){
                        buzzwordTemplate.getGui().getGamePlayScreen().successGuess(currentGuess,i+"");
                    }
                    gc.clearRect(0,0,canvas.getWidth(),canvas.getHeight());
                    drawLines(gc);
                    currentGuess="";
                    buzzwordTemplate.getGui().getGamePlayScreen().setCurentGuessBox(currentGuess);
                    for(HBox h:buzznodes){
                        h.setScaleX(1);h.setScaleY(1);
                        h.getChildren().get(0).setStyle("-fx-text-fill: Black");
                    }
                    event.consume();
                  }
            });
            hBox.setOnMouseMoved(event -> {
                hBox.getChildren().get(0).setStyle("-fx-text-fill: Gold");
            });
            hBox.setOnMouseExited(event -> {
                if(hBox.getScaleX()==1)
                    hBox.getChildren().get(0).setStyle("-fx-text-fill: Black");
            });
        }
        buzzwordTemplate.getGui().getGamePlayScreen().getGameScene().setOnKeyTyped((KeyEvent e)->{
            char guess=e.getCharacter().toLowerCase().charAt(0);
            boolean noGood=true;
            ArrayList newPath;
            if(lastPaths.isEmpty()){
                for(int j=0;j<16;j++){
                    HBox hBox= (HBox) p.getChildren().get(j);
                    if(!((Label)hBox.getChildren().get(0)).getText().isEmpty()) {
                        if(((Label)hBox.getChildren().get(0)).getText().charAt(0)==guess) {
                            newPath=new ArrayList();
                            newPath.add(j);
                            lastPaths.add(newPath);
                            noGood = false;
                            hBox.setScaleX(1.2);hBox.setScaleY(1.2);
                            hBox.getChildren().get(0).setStyle("-fx-text-fill: Gold");
                        }
                    }
                }
                if(noGood)
                    return;
                currentGuess=currentGuess+ guess;
                buzzwordTemplate.getGui().getGamePlayScreen().setCurentGuessBox(currentGuess);
            }
            else {
                ArrayList<ArrayList> lastPositions = new ArrayList();
                for (ArrayList path : lastPaths) {
                    lastPositions.add(path);
                }
                lastPaths.clear();
                for (ArrayList o : lastPositions) {
                    int lastPo = (Integer) o.get(o.size() - 1);
                    boolean noNext=true;
                    for (Object q : adjencyList.get(lastPo+1)) {
                        HBox hBox = (HBox) p.getChildren().get((int) q - 1);
                        if (!((Label) hBox.getChildren().get(0)).getText().isEmpty()) {
                            if (((Label) hBox.getChildren().get(0)).getText().charAt(0) == guess&&!o.contains((int)q-1)) {
                                noNext=false;
                                ArrayList newP=new ArrayList();
                                for (Object t:o){
                                    newP.add(t);
                                }
                                newP.add((int) q - 1);
                                lastPaths.add(newP);

                                hBox.setScaleX(1.2);
                                hBox.setScaleY(1.2);
                                hBox.getChildren().get(0).setStyle("-fx-text-fill: Gold");
                            }
                        }
                    }
                    if(noNext) {
                        highLightPaths(o, gc, Color.BLACK);
                        for(Object l:o){
                            HBox hBox = (HBox) p.getChildren().get((int) l);
                            hBox.setScaleX(1);
                            hBox.setScaleY(1);
                            hBox.getChildren().get(0).setStyle("-fx-text-fill: Black");
                        }
                    }
                }
                if (lastPaths.isEmpty() || currentGuess.length() > 8) {
                    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                    drawLines(gc);
                    currentGuess = "";
                    buzzwordTemplate.getGui().getGamePlayScreen().setCurentGuessBox(currentGuess);
                    lastPaths.clear();
                    for (HBox h : buzznodes) {
                        h.setScaleX(1);
                        h.setScaleY(1);
                        h.getChildren().get(0).setStyle("-fx-text-fill: Black");
                    }
                    return;
                }
                for(ArrayList o:lastPaths){
                    highLightPaths(o, gc, Color.GOLD);
                    for(Object j:o){
                        HBox hBox = (HBox) p.getChildren().get((int) j);
                        hBox.setScaleX(1.2);
                        hBox.setScaleY(1.2);
                        hBox.getChildren().get(0).setStyle("-fx-text-fill: Gold");
                    }
                }
                currentGuess = currentGuess + guess;
                buzzwordTemplate.getGui().getGamePlayScreen().setCurentGuessBox(currentGuess);
                int score = contain(currentGuess);
                if (score != 0&&buzzwordTemplate.getGui().getGamePlayScreen().successGuess(currentGuess,score+"")) {
                    String s=currentGuess;
                    currentGuess="";
                    Timer t = new Timer();
                    t.schedule(new TimerTask() {
                        public void run() {
                            buzzwordTemplate.getGui().getGamePlayScreen().successGuess(s, score + "");
                            lastPaths.clear();
                            gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                            drawLines(gc);
                            currentGuess = "";
                            buzzwordTemplate.getGui().getGamePlayScreen().setCurentGuessBox(currentGuess);
                            for (HBox h : buzznodes) {
                                h.setScaleX(1);
                                h.setScaleY(1);
                                h.getChildren().get(0).setStyle("-fx-text-fill: Black");
                            }
                        }
                    }, 400);

                }
            }

        });
    }
    private void highLightPaths(ArrayList path,GraphicsContext gc,Color c){

        for(int i=0;i<path.size()-1;i++){
            highLightLine((int)path.get(i)+1,(int)path.get(i+1)+1,gc,c);
        }
    }
    private void highLightLine(int i,int j,GraphicsContext gc){
        highLightLine(i,j,gc,Color.GOLD);
    }
    private void highLightLine(int i,int j,GraphicsContext gc,Color c){
        double xI,yI,xJ,yJ;
        GridPane p=buzzwordTemplate.getGui().getGamePlayScreen().getGameDataPane();
        HBox first= (HBox) p.getChildren().get(i-1);
        HBox second=(HBox) p.getChildren().get(j-1);
        xI=first.getLayoutX()+first.getMaxWidth()/2;
        yI=first.getLayoutY()+first.getMaxWidth()/2;
        xJ=second.getLayoutX()+second.getMaxWidth()/2;
        yJ=second.getLayoutY()+second.getMaxWidth()/2;
        gc.setStroke(c);
        gc.setLineWidth(4);
        gc.strokeLine(xI,yI,xJ,yJ);
    }
    private int contain(String currentGuess){
        if(a34.contains(currentGuess))
            if (currentGuess.length()==3)
                return 25;
            else
                return 40;
        if(a56.contains(currentGuess))
            if(currentGuess.length()==5)
                return 50;
            else
                return 65;
        if (a78.contains(currentGuess))
            if(currentGuess.length()==7)
                return 75;
            else
                return 100;
        return 0;
    }
    public String showWords(){
        String s1="";
        for(String s:wordKey)
            s1=s1+s+" ";
        return  s1;
    }
    public void clean(){
        if(!lastPaths.isEmpty())
            for (ArrayList a:lastPaths)
                a.clear();
        lastPaths.clear();
        currentGuess="";
        drawLines(buzzwordTemplate.getGui().getGamePlayScreen().getCanvas().getGraphicsContext2D());
    }
}

