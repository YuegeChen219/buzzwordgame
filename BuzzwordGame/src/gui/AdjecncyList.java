package gui;

import java.util.ArrayList;

/**
 * Created by Yuege on 11/30/2016.
 */
public class AdjecncyList {
    static final ArrayList<ArrayList> adjencyList = new ArrayList<>(17);
    ArrayList adjencyListTemp;
    private AdjecncyList() {
        for (int i = 0; i <= 16; i++) {
            switch (i) {
                case 0:
                case 1:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(2);
                    adjencyListTemp.add(5);
                    adjencyListTemp.add(6);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 2:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(1);
                    adjencyListTemp.add(3);
                    adjencyListTemp.add(5);
                    adjencyListTemp.add(6);
                    adjencyListTemp.add(7);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 3:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(2);
                    adjencyListTemp.add(4);
                    adjencyListTemp.add(6);
                    adjencyListTemp.add(7);
                    adjencyListTemp.add(8);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 4:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(3);
                    adjencyListTemp.add(7);
                    adjencyListTemp.add(8);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 5:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(1);
                    adjencyListTemp.add(2);
                    adjencyListTemp.add(6);
                    adjencyListTemp.add(9);
                    adjencyListTemp.add(10);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 6:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(1);
                    adjencyListTemp.add(2);
                    adjencyListTemp.add(3);
                    adjencyListTemp.add(5);
                    adjencyListTemp.add(7);
                    adjencyListTemp.add(9);
                    adjencyListTemp.add(10);
                    adjencyListTemp.add(11);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 7:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(2);
                    adjencyListTemp.add(3);
                    adjencyListTemp.add(4);
                    adjencyListTemp.add(6);
                    adjencyListTemp.add(8);
                    adjencyListTemp.add(10);
                    adjencyListTemp.add(11);
                    adjencyListTemp.add(12);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 8:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(3);
                    adjencyListTemp.add(4);
                    adjencyListTemp.add(7);
                    adjencyListTemp.add(11);
                    adjencyListTemp.add(12);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 9:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(5);
                    adjencyListTemp.add(6);
                    adjencyListTemp.add(10);
                    adjencyListTemp.add(13);
                    adjencyListTemp.add(14);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 10:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(5);
                    adjencyListTemp.add(6);
                    adjencyListTemp.add(7);
                    adjencyListTemp.add(9);
                    adjencyListTemp.add(11);
                    adjencyListTemp.add(13);
                    adjencyListTemp.add(14);
                    adjencyListTemp.add(15);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 11:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(6);
                    adjencyListTemp.add(7);
                    adjencyListTemp.add(8);
                    adjencyListTemp.add(10);
                    adjencyListTemp.add(12);
                    adjencyListTemp.add(14);
                    adjencyListTemp.add(15);
                    adjencyListTemp.add(16);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 12:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(7);
                    adjencyListTemp.add(8);
                    adjencyListTemp.add(11);
                    adjencyListTemp.add(15);
                    adjencyListTemp.add(16);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 13:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(9);
                    adjencyListTemp.add(10);
                    adjencyListTemp.add(14);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 14:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(9);
                    adjencyListTemp.add(10);
                    adjencyListTemp.add(11);
                    adjencyListTemp.add(13);
                    adjencyListTemp.add(15);
                    adjencyList.add(adjencyListTemp);
                    break;
                case 15:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(10);
                    adjencyListTemp.add(11);
                    adjencyListTemp.add(12);
                    adjencyListTemp.add(14);
                    adjencyListTemp.add(16);
                    adjencyList.add(adjencyListTemp);
                    break;
                default:
                    adjencyListTemp = new ArrayList();
                    adjencyListTemp.add(11);
                    adjencyListTemp.add(12);
                    adjencyListTemp.add(15);
                    adjencyList.add(adjencyListTemp);
                    break;
            }
        }
    }
    public static ArrayList<ArrayList> getAdjencyList(){
        new AdjecncyList();
        return adjencyList;
    }
}
