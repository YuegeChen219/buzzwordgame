package controller;

import gamedata.BuzzwordData;
import gui.Workspace;
import javafx.scene.control.Button;
import template.BuzzwordTemplate;
import ui.AppGui;
import ui.ScreenState;

/**
 * Created by Yuege on 11/8/2016.
 */
public class TheOneAndOnlyController implements BuzzwordController {
    private BuzzwordTemplate buzzwordTemplate;
    private Workspace workspace;
    private BuzzwordData buzzwordData;
    public void goToHelp(){
        buzzwordTemplate.getGui().getPrimaryStage().setScene(buzzwordTemplate.getGui().getHomeScreen().getHelpScene());
    }
    public TheOneAndOnlyController(BuzzwordTemplate buzzwordTemplate){
        this.buzzwordTemplate=buzzwordTemplate;
        buzzwordData=new BuzzwordData();
        workspace=new Workspace(buzzwordTemplate,buzzwordData);
    }
    public BuzzwordTemplate getTemplate(){return buzzwordTemplate;}

    public void goHomeScreen(){
        buzzwordTemplate.getGui().getPrimaryStage().setScene(buzzwordTemplate.getGui().getHomeScreen().getHomeScene());
    }
    public void goToLevelSelectionScreen(){
        buzzwordTemplate.getGui().getPrimaryStage().setScene(buzzwordTemplate.getGui().getLevelSelectionScreen().getLevelSelectionScene());
    }
    public void goToGamePlayScreen(){
        buzzwordTemplate.getGui().getGamePlayScreen().setNext();
        buzzwordTemplate.getGui().getPrimaryStage().setScene(buzzwordTemplate.getGui().getGamePlayScreen().getGameScene());
    }
    public String getMode(){return buzzwordTemplate.getGui().getHomeScreen().getModeString();}
    public void setMode(){buzzwordTemplate.getGui().getLevelSelectionScreen().setModeLabel(this.getMode());}
    public void updateAppGui(AppGui gui){buzzwordTemplate.setGui(gui);}
    public void setGamePlayLabel(String mode,String level,String target){
        int i=Integer.parseInt(target);
        switch (i){
            case 1:i=100;break;
            case 2:i=125;break;
            case 3:i=150;break;
            case 4:i=175;break;
            default: i=200;break;
        }
        buzzwordTemplate.getGui().getGamePlayScreen().setLabel(mode,level,i,target);
    }

    public boolean createNewProfile(String username,String password){
        boolean b=buzzwordData.createNewProfile(username,password);
        workspace.renderUserInformation();
        return b;
    }
    public boolean logIn(String username,String password){
        boolean b=buzzwordData.logIn(username,password);
        workspace.renderUserInformation();
        return b;
    }

    public void renderLevelSelection(){
        workspace.renderLevelSelection();
    }
    public void renderGamePlayScreen(String s1,String s2){
        buzzwordTemplate.getGui().getGamePlayScreen().clean();
        workspace.readWord(s1);
        workspace.renderGamePlayScreen(s2);
    }
    public void refresh(String level){
        buzzwordTemplate.getGui().getGamePlayScreen().clean();
        workspace.renderGamePlayScreen(level);
        workspace.renderPlayGui(true);
    }
    public void play(boolean play){
        workspace.renderPlayGui(play);
    }
    public String showWords(){
        return workspace.showWords();
    }
    public void saveUserProgress(String mode,String level,int newRecord){
        buzzwordData.saveUserProgress(mode,level,newRecord);
        workspace.renderLevelSelection();
    }
    public void drawLines(){
        workspace.drawLines(buzzwordTemplate.getGui().getGamePlayScreen().getCanvas().getGraphicsContext2D());
    }
    public void clean(){workspace.clean();}
    public String getUserInfo(){return buzzwordData.getUserInformation();}
    public void setProfile(ScreenState s,boolean playing){
        buzzwordTemplate.getGui().getHomeScreen().setSs(s);
        buzzwordTemplate.getGui().getHomeScreen().setPlaying(playing);
        buzzwordTemplate.getGui().getHomeScreen().setUserinformation(buzzwordData.getUserInformation());
        buzzwordTemplate.getGui().getPrimaryStage().setScene(buzzwordTemplate.getGui().getHomeScreen().getProfileScene());
    }
    public void resetTimer(){buzzwordTemplate.getGui().getGamePlayScreen().resetTimer();}
    public void resume(){buzzwordTemplate.getGui().getGamePlayScreen().resume();}
    public boolean setUsername(String s){return buzzwordData.setUsername(s);}
    public boolean setPassword(String s1,String s2){return buzzwordData.setPassword(s1,s2);}
    public void setName(){
        buzzwordTemplate.getGui().getHomeScreen().getProfileButton().setText(buzzwordData.getUsername());
        buzzwordTemplate.getGui().getLevelSelectionScreen().getProfileButton().setText(buzzwordData.getUsername());
        buzzwordTemplate.getGui().getGamePlayScreen().getProfileButton().setText(buzzwordData.getUsername());
    }
    public void nextLevel(String level){
        int l=Integer.parseInt(level);
        ((Button)(buzzwordTemplate.getGui().getLevelSelectionScreen().getLevelNodes().getChildren().get(l))).fire();
    }
    public int getRecord(String mode,String level){
        return buzzwordData.getRecord(mode,level);
    }
}
