package template;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;
import ui.AppGui;
import ui.LogInOffDialog;
import ui.MessageDialog;
import ui.YesNoCancelDialog;

/**
 * Created by Yuege on 11/8/2016.
 */
public class BuzzwordTemplate extends Application {

    private AppGui gui;

    public void start(Stage primaryStage){
        LogInOffDialog logDialog=LogInOffDialog.getSingleton();
        logDialog.init(primaryStage);
        MessageDialog messageDialog=MessageDialog.getSingleton();
        messageDialog.init(primaryStage);
        YesNoCancelDialog yesNoCancelDialog=YesNoCancelDialog.getSingleton();
        yesNoCancelDialog.init(primaryStage);
        primaryStage.setTitle("!! Buzzword !!");

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());


        gui = new AppGui(primaryStage,bounds.getHeight(),bounds.getWidth(),this,"!! Buzzword !!");

    }
    public AppGui getGui(){return gui;}
    public void setGui(AppGui gui){this.gui=gui;}
}
