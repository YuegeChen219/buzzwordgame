package controller;

import ui.AppGui;
import ui.ScreenState;

/**
 * Created by Yuege on 11/8/2016.
 */
public interface BuzzwordController {
    void goHomeScreen();
    void goToLevelSelectionScreen();
    void goToGamePlayScreen();
    void updateAppGui(AppGui gui);
    void setMode();
    String getMode();
    void setGamePlayLabel(String s,String s1,String s3);
    boolean createNewProfile(String s1,String s2);
    boolean logIn(String s1,String s2);
    void renderGamePlayScreen(String s1,String s2);
    void refresh(String s);
    void play(boolean play);
    String showWords();
    void saveUserProgress(String s1,String s2,int i);
    void renderLevelSelection();
    void drawLines();
    void clean();
    void goToHelp();
    String getUserInfo();
    void setProfile(ScreenState screenState,boolean playing);
    void resume();
    void resetTimer();
    boolean setUsername(String s);
    boolean setPassword(String s1,String s2);
    void setName();
    void nextLevel(String level);
    int getRecord(String s,String t);
}
