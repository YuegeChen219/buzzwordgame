package ui;

import controller.BuzzwordController;
import data.BuzzwordDataComponent;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;
import timer.BuzzwordTimerTask;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Timer;

import static ui.GameState.*;
import static ui.ScreenState.PLAY;

/**
 * Created by Yuege on 11/8/2016.
 */
public class GamePlayScreen {
    private Scene gameScene;
    private Timer gameTimer;
    private BuzzwordTimerTask gameTimerTask;
    private BorderPane gamePane;
    private FlowPane toolBar;
    private FlowPane gameScoreBar;
    private GridPane gameDataPane;
    private Button profileButton;
    private Button homeButton;
    private Button playPauseButton;
    private Label levelLabel;
    private Label modeLabel;
    private Label buzzwordLabel;
    private HBox timeRemainingBox;
    private Button levelButton;
    private HBox curentGuessBox;
    private VBox targetScoreBox;
    private Button quitButton;
    private Button refreshButton;
    private Button nextButton;
    private int targetPoint=100;
    private Text targetPoints;
    private final TableView<BuzzwordDataComponent> scoreTable=new TableView();
    private final ObservableList<BuzzwordDataComponent> data =
            FXCollections.observableArrayList(
                    new BuzzwordDataComponent("TOTAL","0")
            );
    private BuzzwordController buzzwordController;
    private MessageDialog messageDialog=MessageDialog.getSingleton();
    private YesNoCancelDialog yesNoCancelDialog=YesNoCancelDialog.getSingleton();
    private GameState gameState=UNSTARTED;
    private int t;
    private Text time;
    private Canvas canvas;
    TableColumn<BuzzwordDataComponent,String> totalCol;
    private String timeLimit="40";
    private VBox headBox;
    private final ScreenState ss=PLAY;
    private boolean paused;
    public GamePlayScreen(BuzzwordController buzzwordController){
        this.buzzwordController=buzzwordController;
        initGui();
        initHandler();
        gameScene=new Scene(gamePane);
        gameScene.setOnKeyReleased(event -> {
            if(event.getCode()== KeyCode.H&&event.isControlDown())
                homeButton.fire();
            if(event.getCode()== KeyCode.Q&&event.isControlDown())
                quitButton.fire();
        });
    }
    public void resetTimer(){
        time.setText("Time Remaining: "+timeLimit+" seconds");
        gameState=WIN;
        playPauseButton.fire();
        if(gameTimer!=null)
            gameTimer.cancel();
    }
    public void resume(){
        if(gameState==PAUSED&&paused)
            playPauseButton.fire();
        paused=false;
    }
    public void initHandler(){
        nextButton.setOnAction(event -> {
            buzzwordController.nextLevel(levelLabel.getText().substring(6,7));
            nextButton.setVisible(false);
        });
        profileButton.setOnAction(event -> {
            paused=false;
            if(gameState==INPROGRESS){
                playPauseButton.fire();
                paused=true;
            }
            boolean start;
            if(gameState!=UNSTARTED)
                start=true;
            else
                start=false;
            buzzwordController.setProfile(ss,start);

        });
        homeButton.setOnAction(event -> {
            if(gameState!=UNSTARTED){
                boolean paused=false;
                if(gameState==INPROGRESS){
                    playPauseButton.fire();
                    paused=true;
                }
                yesNoCancelDialog.show("go Home?","Do you want to go to home screen? Current progress will be discarded");
                String s=yesNoCancelDialog.getSelection();
                if(s.equals("Yes")){
                    resetTimer();
                    buzzwordController.goHomeScreen();
                }
                else if(gameState==PAUSED&&paused)
                    playPauseButton.fire();
            }
            else
                buzzwordController.goHomeScreen();
            }

        );
        levelButton.setOnAction(event -> {
            if(gameState!=UNSTARTED){
                boolean paused=false;
                if(gameState==INPROGRESS){
                    playPauseButton.fire();
                    paused=true;
                }
                yesNoCancelDialog.show("select level?","Do you want to select level again? Current progress will be discarded");
                String s=yesNoCancelDialog.getSelection();
                if(s.equals("Yes")){
                    resetTimer();
                    buzzwordController.goToLevelSelectionScreen();
                }
                else if(gameState==PAUSED&&paused)
                    playPauseButton.fire();
            }
            else {
                buzzwordController.goToLevelSelectionScreen();
            }

        }
        );
        quitButton.setOnAction(event -> {
            boolean paused=false;
            if(gameState==INPROGRESS){
                playPauseButton.fire();
                paused=true;
            }
            yesNoCancelDialog.show("Quit","Do you really want to quit the game? System will save any progress you have made.");
            String s=yesNoCancelDialog.getSelection();
            if(s.equals("Yes")){
                System.exit(0);
            }
            else if(gameState==PAUSED&&paused)
                playPauseButton.fire();
        });

        playPauseButton.setOnAction(event -> {
            if(gameState==UNSTARTED){
                nextButton.setVisible(false);
                refreshButton.setDisable(false);
                buzzwordController.renderGamePlayScreen(modeLabel.getText(),levelLabel.getText().substring(6,7));
                buzzwordController.play(true);
                gameTimer=new Timer();
                initTask();
                gameTimer.schedule(gameTimerTask,1000,1000);
                playPauseButton.setText("Pause");
                gameState=INPROGRESS;
            }
            else if(gameState==INPROGRESS){
                playPauseButton.setText("Resume");
                gameTimer.cancel();
                gameTimer=new Timer();
                initTask();
                gameState=PAUSED;
                refreshButton.setDisable(true);
                for (int i=0;i<16;i++)
                    ((HBox) (gameDataPane.getChildren().get(i))).getChildren().get(0).setVisible(false);
                buzzwordController.play(false);
            }
            else if(gameState==PAUSED) {
                playPauseButton.setText("Pause");
                gameTimer.schedule(gameTimerTask,1000,1000);
                gameState=INPROGRESS;
                refreshButton.setDisable(false);
                for (int i=0;i<16;i++)
                    ((HBox) (gameDataPane.getChildren().get(i))).getChildren().get(0).setVisible(true);
                buzzwordController.play(true);
            }
            else if(gameState==LOSS){
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        messageDialog.show("LOSS","Unluckily, you lose. Try next Time!\n The suggested words are: "+buzzwordController.showWords());
                        playPauseButton.setText("Replay");
                        gameState=UNSTARTED;
                        buzzwordController.clean();
                        buzzwordController.play(false);
                        refreshButton.setDisable(true);
                        reloadScoreTable();
                        for (int i=0;i<16;i++)
                            addText("",((HBox) (gameDataPane.getChildren().get(i))));
                    }
                });
            }
            else {
                playPauseButton.setText("Replay");
                gameState=UNSTARTED;
                reloadScoreTable();
                buzzwordController.clean();
                buzzwordController.play(false);
                refreshButton.setDisable(true);
                for (int i=0;i<16;i++)
                    addText("",((HBox) (gameDataPane.getChildren().get(i))));
            }

        });
        refreshButton.setOnAction(event -> {
            yesNoCancelDialog.show("Restart","Do you want to restart the game at current level? Current progress will " +
                    "be discarded.");
            String s=yesNoCancelDialog.getSelection();
            if(s.equals("Yes")){
                time.setText("Time Remaining: "+timeLimit+" seconds");
                gameState=WIN;
                playPauseButton.fire();
                gameTimer.cancel();
                playPauseButton.fire();
            }
        });
    }
    public void setLabel(String mode,String level,int i,String levelNum){
        modeLabel.setText(mode);
        levelLabel.setText(level);
        playPauseButton.setText("Play");
        setTarget(i);
        int l=Integer.parseInt(levelNum);
        timeLimit=(40+(l-1)*5)+"";
        time.setText("Time Remaining: "+timeLimit+" seconds");
    }
    public void initGui(){
        initToolBar();
        initGameScoreBar();
        gamePane=new BorderPane();
        gamePane.setLeft(toolBar);
        gamePane.setRight(gameScoreBar);

        headBox=new VBox();
        buzzwordLabel=new Label("!!BuzzWord!!");
        buzzwordLabel.setStyle(" -fx-font-size:50; -fx-text-fill: White");
        modeLabel=new Label();
        modeLabel.setText(buzzwordController.getMode());
        modeLabel.setStyle(" -fx-font-size:50; -fx-text-fill: rgb(20, 171, 192)");
        headBox.getChildren().addAll(buzzwordLabel,modeLabel);
        modeLabel.setTranslateY(60);
        buzzwordLabel.setAlignment(Pos.CENTER);
        headBox.setAlignment(Pos.TOP_CENTER);
        gamePane.setTop(headBox);

        Group root=new Group();

        gameDataPane=new GridPane();
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                HBox buzznode=new HBox();
                buzznode.setShape(new Circle(40, Color.WHITE));
                buzznode.setMaxSize(80,80);
                buzznode.setMinSize(80,80);
                buzznode.setStyle(" -fx-border-radius: 40; -fx-border-width: 3;-fx-border-color: rgb(244, 242, 92);-fx-background-color: White; -fx-font-size: 30pt;");
                gameDataPane.add(buzznode,j,i);
            }
        }
        gameDataPane.setHgap(50);
        gameDataPane.setVgap(50);
        gameDataPane.setAlignment(Pos.CENTER);
        canvas=new Canvas(500,500);
        root.getChildren().addAll(canvas,gameDataPane);
        gamePane.setCenter(root);
        root.setTranslateX(30);
        levelLabel=new Label("Level");
        levelLabel.setStyle(" -fx-font-size:50; -fx-text-fill: rgb(20, 171, 192); ");
        gamePane.setBottom(levelLabel);
        levelLabel.setAlignment(Pos.CENTER);
        levelLabel.setTranslateX(720);
        levelLabel.setTranslateY(-30);
        Image saberLily=new Image( getClass().getClassLoader().getResource("images"+File.separator+"Touka.png").toExternalForm());
        BackgroundImage backgroundImage = new BackgroundImage( saberLily, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);
        gamePane.setBackground(background);
    }
    public void clean(){
        for (int i=0;i<16;i++)
         addText("", (HBox) gameDataPane.getChildren().get(i));
    }
    public Canvas getCanvas(){return canvas;}
    public GridPane getGameDataPane(){return gameDataPane;}
    public void addText(String x,HBox buzznode){
        if(x.equals("")){
            buzznode.setScaleX(1);
            buzznode.setScaleY(1);
        }
        Label b=new Label(x.toLowerCase());
        b.setTextFill(Color.BLACK);
        buzznode.getChildren().setAll(b);
        b.setAlignment(Pos.CENTER);
        b.setTranslateX(27);
        b.setTranslateY(12);
    }
    public void initToolBar(){
        profileButton=initButtonText("Username");
        levelButton=initButtonText("Select Level");
        playPauseButton=initButtonText("Play!");
        homeButton=initButtonView("home.png");
        quitButton=initButtonView("quit2.png");
        refreshButton=initButtonView("refresh.png");
        nextButton=initButtonView("next.png");
        refreshButton.setDisable(true);
        quitButton.setClip(new Ellipse(70,65,57,57));
        quitButton.setTranslateY(350);
        quitButton.setTranslateX(175);
        profileButton.setTranslateY(-150);
        levelButton.setTranslateY(-145);
        playPauseButton.setTranslateY(-140);
        toolBar=new FlowPane();
        toolBar.getChildren().addAll(quitButton,profileButton,levelButton,playPauseButton,homeButton, refreshButton,nextButton);
        nextButton.setTranslateX(130);
        nextButton.setVisible(false);
        refreshButton.setTranslateX(625);
        homeButton.setTranslateY(-90);
        toolBar.setAlignment(Pos.CENTER_LEFT);
        toolBar.setStyle("-fx-background-color: transparent;");
        toolBar.setTranslateX(148);
        toolBar.setTranslateY(77);
    }
    public Button initButtonView(String name){
        Button button=new Button();
        URL imgDirURL = getClass().getClassLoader().getResource("images"+ File.separator+name);
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()))) {
            Image buttonImage=new Image(imgInputStream);
            button.setGraphic(new ImageView(buttonImage));
            Circle c=new Circle(buttonImage.getWidth()/2+8,buttonImage.getHeight()/2+5,buttonImage.getWidth()/2);
            button.setClip(c);
            if(name.equals("refresh.png")||name.equals("next.png")){
                c=new Circle(buttonImage.getWidth()/2+8,buttonImage.getHeight()/2+5,buttonImage.getWidth()/2-5);
                button.setClip(c);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        button.setOnMouseEntered(event -> {button.setScaleX(1.2);button.setScaleY(1.2);});
        button.setOnMouseExited(event -> {button.setScaleX(1);button.setScaleY(1);});
        return button;
    }
    public Button initButtonText(String name){
        Button button=new Button(name);
        button.setPrefSize(340,100);
        button.setStyle("-fx-background-color:rgb(58, 58, 58); -fx-text-fill: rgb(198, 198, 198);-fx-font: bold italic 35 Arial;" +
                " -fx-border-radius: 40; -fx-border-width: 3;-fx-border-color: rgb(244, 242, 92); -fx-background-radius: 40");
        button.setOnMouseEntered(event -> {button.setScaleX(1.2);button.setScaleY(1.05);});
        button.setOnMouseExited(event -> {button.setScaleX(1);button.setScaleY(1);});
        return button;
    }

    public void initTask(){
        gameTimerTask=new BuzzwordTimerTask() {
            public void run() {
                t=Integer.parseInt( time.getText().substring(16,18));
                if(t==0){
                    time.setText("Time Remaining: "+timeLimit+" seconds");
                    gameState=LOSS;
                    playPauseButton.fire();
                    gameTimer.cancel();
                }

                else if(t<=10)
                    time.setText("Time Remaining: 0"+--t+" seconds");

                else
                    time.setText("Time Remaining: "+--t+" seconds");
            }
        };
    }

    public void initGameScoreBar(){
        gameScoreBar=new FlowPane();

        time=new Text("Time Remaining: "+timeLimit+" seconds");
        time.setFill(Color.RED);
        time.setStyle("-fx-text-fill: Red ;-fx-font: bold italic 30 Arial;");
        time.setTranslateY(20);
        initTask();
        timeRemainingBox=new HBox();
        timeRemainingBox.getChildren().add(time);
        timeRemainingBox.setStyle("-fx-background-color: gray; -fx-border-radius: 40; -fx-border-width: 3; -fx-background-radius: 40");
        timeRemainingBox.setPrefSize(200,80);
        totalCol= new TableColumn<>("WordGuessed");
        totalCol.setStyle(".table-row-cell:odd{\n" +
                "    -fx-background-color: Black;\n" + "}");
        totalCol.setMinWidth(150);
        totalCol.setStyle("-fx-back-ground-color: transparent;-fx-text-fill: White;");
        TableColumn<BuzzwordDataComponent,String> scoreCol = new TableColumn<>("Score");
        scoreCol.setMinWidth(148);
        scoreCol.setStyle("-fx-back-ground-color: transparent;-fx-text-fill: White;");
        scoreTable.getColumns().addAll(totalCol,scoreCol);
        totalCol.setCellValueFactory(
                new PropertyValueFactory<>("word")
        );
        scoreCol.setCellValueFactory(
                new PropertyValueFactory<>("score")
        );
        scoreTable.setItems(data);
        scoreTable.setFixedCellSize(40);
        scoreTable.setPrefSize(300,440);
        scoreTable.setStyle(" -fx-background-color: transparent; -fx-font-size: 20 ; -fx-text-fill: White;");
        totalCol.setCellFactory(column -> {
            return new TableCell<BuzzwordDataComponent, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    setText(empty ? "" : getItem().toString());
                    setGraphic(null);
                    TableRow<BuzzwordDataComponent> currentRow = getTableRow();

                    if (!isEmpty()) {
                        if(item.equals("TOTAL"))
                            currentRow.setStyle("-fx-background-color: rgb(0, 72, 72)");
                        else
                            currentRow.setStyle("-fx-background-color:lightcoral");
                    }
                }
            };
        });
        scoreTable.setEditable(false);
        targetScoreBox =new VBox();
        Text target=new Text("Target:");
        target.setFill(Color.WHITE);
        target.setStyle("-fx-text-fill: Red ;-fx-font: bold italic 30 Arial;");
        targetPoints=new Text(targetPoint+" Points");
        targetPoints.setFill(Color.WHITE);
        targetPoints.setStyle("-fx-text-fill: Red ;-fx-font: bold italic 30 Arial;");
        targetScoreBox.setStyle("-fx-background-color: gray; -fx-border-radius: 40; -fx-border-width: 3; -fx-background-radius: 40");
        targetScoreBox.setPrefSize(260,160);
        targetScoreBox.setTranslateX(0);
        targetScoreBox.setTranslateY(13);
        targetScoreBox.getChildren().addAll(target,targetPoints);
        target.setTranslateY(40);
        target.setTranslateX(50);
        targetPoints.setTranslateY(40);
        targetPoints.setTranslateX(50);
        curentGuessBox=new HBox();
        curentGuessBox.setPrefSize(400,70);
        Text currentGuess=new Text("CurrentGuess: \n  ");
        currentGuess.setFill(Color.WHITE);
        curentGuessBox.getChildren().addAll(currentGuess);
        curentGuessBox.setStyle("-fx-background-color: lightcoral;-fx-font: bold italic 30 Arial;");
        gameScoreBar.getChildren().addAll(timeRemainingBox,curentGuessBox,scoreTable,targetScoreBox);
        timeRemainingBox.setTranslateY(-12);
        timeRemainingBox.setTranslateX(-5);
        gameScoreBar.setTranslateY(-91);
    }
    public Button getProfileButton(){return profileButton;}
    public Scene getGameScene(){return gameScene;}
    public void setTarget(int i){
        targetPoint=i;
        targetPoints.setText(targetPoint+" Points");
    }
    public void setCurentGuessBox(String s){
        ( (Text)(curentGuessBox.getChildren().get(0))).setText("CurrentGuess: \n"+s);
    }
    public void reloadScoreTable(){
        data.setAll(new BuzzwordDataComponent("TOTAL","0"));
        totalCol.setCellFactory(column -> {
            return new TableCell<BuzzwordDataComponent, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    setText(empty ? "" : getItem().toString());
                    setGraphic(null);
                    TableRow<BuzzwordDataComponent> currentRow = getTableRow();

                    if (!isEmpty()) {
                        if(item.equals("TOTAL"))
                            currentRow.setStyle("-fx-background-color: rgb(0, 72, 72)");
                        else
                            currentRow.setStyle("-fx-background-color:lightcoral");
                    }
                }
            };
        });
    }
    public boolean successGuess(String word,String score){
        for(BuzzwordDataComponent b:data)
            if(b.getWord().equals(word))
                return false;
        data.add(data.size()-1,new BuzzwordDataComponent(word,score));
        int newScore=Integer.parseInt(data.get(data.size()-1).getScore())+Integer.parseInt(score);
        data.get(data.size()-1).setScore(newScore+"");

        if(newScore>=targetPoint){
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    if(!levelLabel.getText().substring(6,7).equals("5"))
                        nextButton.setVisible(true);

                    gameTimer.cancel();
                    int record=Integer.parseInt(timeLimit)-Integer.parseInt(time.getText().substring(16,18));
                    int oldRecord=buzzwordController.getRecord(modeLabel.getText(),levelLabel.getText().substring(6,7));
                    time.setText("Time Remaining: "+timeLimit+" seconds");
                    messageDialog.show("VICTORY!","Congratulation! You have completed this level!"+
                            "\nThe suggested words are: "+buzzwordController.showWords());
                    if(oldRecord==0||record<oldRecord){
                        messageDialog.show("New Personal Best!","Congratulation! You just set a new record at this level: "+record+" seconds!");
                        buzzwordController.saveUserProgress(modeLabel.getText(),levelLabel.getText().substring(6,7),record);
                    }
                    else
                        buzzwordController.saveUserProgress(modeLabel.getText(),levelLabel.getText().substring(6,7),oldRecord);
                    gameState=WIN;
                    playPauseButton.fire();
                }
            });
        }
        return true;
    }
    public void setNext(){nextButton.setVisible(false);}
}
