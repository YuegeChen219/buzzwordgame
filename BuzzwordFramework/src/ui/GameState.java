package ui;

/**
 * Created by Yuege on 11/26/2016.
 */
public enum GameState {
    UNSTARTED,
    INPROGRESS,
    PAUSED,
    WIN,
    LOSS,
}