package ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
/**
 * Created by Yuege on 11/8/2016.
 */
public class LogInOffDialog extends Stage {
    static LogInOffDialog singleton = null;

    VBox messagePane;
    Scene messageScene;
    Button confirmButton;
    Button closeButton;
    String username;
    String passWord;
    final TextField name = new TextField();
    final TextField password= new PasswordField();
    HBox passwordBox;
    HBox nameBox;
    private Label nameLabel,passwordLabel;

    private LogInOffDialog() { }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static LogInOffDialog getSingleton() {
        if (singleton == null)
            singleton = new LogInOffDialog();
        return singleton;
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        // LABEL TO DISPLAY THE CUSTOM MESSAGE

        name.setPromptText("Username:");
        name.setOnKeyReleased((KeyEvent e)->{
            if(e.getCode()== KeyCode.ENTER&&!((name.getText().isEmpty())||password.getText().isEmpty())){
                confirmButton.fire();
            }
        });
        password.setOnKeyReleased((KeyEvent e)->{
            if(e.getCode()== KeyCode.ENTER&&!((name.getText().isEmpty())||password.getText().isEmpty())){
                confirmButton.fire();
            }
        });
        password.setPromptText("Password:");
        // CLOSE BUTTON
        closeButton = new Button("Cancel");
        confirmButton=new Button("Confirm");
        closeButton.setOnAction(e -> {
            passwordBox.setVisible(true);
            password.setPromptText("Password:");
            name.setPromptText("Username:");
            nameLabel.setText("Username: ");
            passwordLabel.setText("Password: ");
            name.clear();
            password.clear();
            this.close();
        });
        confirmButton.setOnAction( e->{
            passwordBox.setVisible(true);
            password.setPromptText("Password:");
            name.setPromptText("Username:");
            nameLabel.setText("Username: ");
            passwordLabel.setText("Password: ");
            if(!name.getText().isEmpty())
                username=name.getText();
            if(!password.getText().isEmpty())
                passWord=password.getText();
            name.clear();
            password.clear();
            this.close();});
        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        nameLabel=new Label("Username: ");
        nameBox=new HBox();
        nameBox.getChildren().addAll(nameLabel,name);
        messagePane.getChildren().add(nameBox);
        passwordBox=new HBox();
        passwordLabel=new Label("Password: ");
        passwordBox.getChildren().addAll(passwordLabel,password);
        messagePane.getChildren().add(passwordBox);
        HBox buttonBox=new HBox();
        buttonBox.getChildren().addAll(confirmButton,closeButton);
        messagePane.getChildren().add(buttonBox);

        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(80, 60, 80, 60));
        messagePane.setSpacing(20);
        messagePane.setStyle("-fx-background-color:  linear-gradient(to right, rgb(144, 144, 144) 2%,rgb(83, 0, 0) 49%, rgb(0, 42, 42) 98%);");
        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }
    public String getUserName(){
        String s=username;
        username=null;
        return s;
    }
    public String getPassword(){
        String s=passWord;
        passWord=null;
        return s;
    }
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window.
     */
    public void show(String title) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);

        // SET THE MESSAGE TO DISPLAY TO THE USER

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }
    public void show(String title,boolean b) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);
        passwordBox.setVisible(b);
        if(b){
            nameLabel.setText("old password: ");
            name.setPromptText("old password");
            passwordLabel.setText("new password: ");
            password.setPromptText("new password");
        }
        // SET THE MESSAGE TO DISPLAY TO THE USER

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }
}
