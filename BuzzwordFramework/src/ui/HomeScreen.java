package ui;

import controller.BuzzwordController;
import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.util.Callback;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ui.ScreenState.HOME;
import static ui.ScreenState.PLAY;

/**
 * Created by Yuege on 11/8/2016.
 */
public class HomeScreen {
    private Scene homeScene;
    private Scene profileScene;
    private BorderPane homePane;
    private Scene helpScene;
    private VBox toolBar;
    private GridPane buzzwordNodes;
    private Label buzzwordLabel;
    private Button newProfileButton;
    private Button logInButton;
    private Button startPlayButton;
    private Button helpButton;
    private Button profileButton;
    private Button quitButton;
    private BuzzwordController buzzwordController;
    private final ComboBox modeComboBox=new ComboBox();
    private Label modeLabel;
    private String mode="Animals";
    private ScreenState ss=HOME;
    private LogInOffDialog logDialog=LogInOffDialog.getSingleton();
    private MessageDialog messageDialog=MessageDialog.getSingleton();
    private YesNoCancelDialog yesNoCancelDialog=YesNoCancelDialog.getSingleton();
    private boolean logIn=false;
    private boolean playing=false;
    private Text userinformation;
    public HomeScreen(BuzzwordController buzzwordController)  {
        this.buzzwordController=buzzwordController;
        initGui();
        initHandler();
        homeScene=new Scene(homePane);
        homeScene.setOnKeyReleased((KeyEvent event) -> {
            if(event.getCode()==KeyCode.L&&event.isControlDown())
                if (logInButton.isVisible())
                    logInButton.fire();
            if(event.getCode()==KeyCode.P&&event.isControlDown()&&event.isShiftDown())
                if(newProfileButton.isVisible())
                    newProfileButton.fire();
            if(event.getCode()==KeyCode.P&&event.isControlDown()&&!event.isShiftDown())
                if(startPlayButton.isVisible())
                    startPlayButton.fire();
            if(event.getCode()==KeyCode.Q&&event.isControlDown())
                quitButton.fire();
        });
        initHelpScene();
        initProfileScene();
    }

    private void initProfileScene() {
        Image saberLily=new Image( getClass().getClassLoader().getResource("images"+File.separator+"Touka.png").toExternalForm());
        BackgroundImage backgroundImage = new BackgroundImage( saberLily, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);
        BorderPane profilePane=new BorderPane();
        profilePane.setBackground(background);
        Button home=new Button();
        URL imgDirURL = getClass().getClassLoader().getResource("images"+ File.separator+"home.png");
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()))) {
            Image buttonImage=new Image(imgInputStream);
            home.setGraphic(new ImageView(buttonImage));
            Circle c=new Circle(buttonImage.getWidth()/2+8,buttonImage.getHeight()/2+5,buttonImage.getWidth()/2);
            home.setClip(c);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        home.setOnMouseEntered(event -> {home.setScaleX(1.2);home.setScaleY(1.2);});
        home.setOnMouseExited(event -> {home.setScaleX(1);home.setScaleY(1);});
        home.setOnAction(event -> {
            if(playing){
                yesNoCancelDialog.show("go Home?","Your game is in progress! Do you still want to go to home screen? Current progress will be discarded");
                String s=yesNoCancelDialog.getSelection();
                if(s.equals("Yes")&&ss==PLAY){
                    buzzwordController.resetTimer();
                    buzzwordController.goHomeScreen();
                }
            }
            else
                buzzwordController.goHomeScreen();
        });
        Button goBack=initButtonText("GO BACK");
        goBack.setTranslateY(30);
        goBack.setOnAction(event -> {
            switch (ss){
                case HOME:
                    buzzwordController.goHomeScreen();break;
                case LEVEL:
                    buzzwordController.goToLevelSelectionScreen();ss=HOME;break;
                case PLAY:
                    buzzwordController.resume();ss=HOME;
                    buzzwordController.goToGamePlayScreen();break;
            }
        });
        HBox headBox=new HBox();
        headBox.getChildren().addAll(home,goBack);
        profilePane.setTop(headBox);
        userinformation=new Text();
        userinformation.setText(buzzwordController.getUserInfo());
        userinformation.setFill(Color.GOLD);
        userinformation.setStyle(" -fx-font-size:35;");
        HBox hBox=new HBox();
        Button changeUserName=initButtonText("Change Name");
        Button changePassword=initButtonText("Change Password");
        Button logOut=initButtonText("Log Out");
        logOut.setOnAction(event -> {
            yesNoCancelDialog.show("Log Out","Do you really want log out? System will save any progress you have made.");
            String s=yesNoCancelDialog.getSelection();
            if(s.equals("Yes")&&ss==PLAY){
                buzzwordController.resetTimer();
                buzzwordController.goHomeScreen();
                updateGui(false);
            }
            else if(s.equals("Yes")&&ss!=PLAY){
                buzzwordController.goHomeScreen();
                updateGui(false);
            }
        });
        changeUserName.setOnAction(event -> {
            logDialog.show("Please enter new username",false);
            String name=logDialog.getUserName();
            if(!(name==null)){
                if(buzzwordController.setUsername(name)) {
                    messageDialog.show("Change username", "Success!");
                    buzzwordController.setName();
                    buzzwordController.setProfile(ss,playing);
                }
                else
                    messageDialog.show("Username already taken", "Failure: This user already exists.");
            }
        });
        changePassword.setOnAction(event -> {
            logDialog.show("Enter old password and new password",true);
            String name=logDialog.getUserName();
            String password=logDialog.getPassword();
            if(!(name==null||password==null)){
                if(buzzwordController.setPassword(name,password)) {
                    messageDialog.show("Change password", "Success!");
                }
                else
                    messageDialog.show("Incorrect old password", "Failure: Old password is incorrect.");
            }
        });
        hBox.getChildren().addAll(changeUserName,changePassword,logOut);
        VBox body=new VBox();
        body.getChildren().addAll(userinformation,hBox);
        profilePane.setCenter(body);
        headBox.setTranslateX(400);
        body.setTranslateX(400);
        profileScene=new Scene(profilePane);
        profileScene.setOnKeyReleased(event -> {
            if(event.getCode()==KeyCode.H&&event.isControlDown())
                home.fire();
        });
    }

    private void initHelpScene() {
        try {
            Image saberLily=new Image( getClass().getClassLoader().getResource("images"+File.separator+"Touka.png").toExternalForm());
            BackgroundImage backgroundImage = new BackgroundImage( saberLily, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
            Background background = new Background(backgroundImage);
            BorderPane helpPane=new BorderPane();
            helpPane.setBackground(background);
            Text helpText=new Text();
            URL imgDirURL = getClass().getClassLoader().getResource("wordlist" + File.separator  + "help.txt");
            File txt = new File(imgDirURL.toURI());
            InputStreamReader read = new InputStreamReader(new FileInputStream(txt));
            BufferedReader bufferedReader = new BufferedReader(read);
            String lineTxt;
            String s="";
            while ((lineTxt = bufferedReader.readLine()) != null) {
                s=s+lineTxt+"\r\n";
            }
            helpText.setText(s);
            Button homeButton=new Button();
            imgDirURL = getClass().getClassLoader().getResource("images"+ File.separator+"home.png");
            try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()))) {
                Image buttonImage=new Image(imgInputStream);
                homeButton.setGraphic(new ImageView(buttonImage));
                Circle c=new Circle(buttonImage.getWidth()/2+8,buttonImage.getHeight()/2+5,buttonImage.getWidth()/2);
                homeButton.setClip(c);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            homeButton.setOnMouseEntered(event -> {homeButton.setScaleX(1.2);homeButton.setScaleY(1.2);});
            homeButton.setOnMouseExited(event -> {homeButton.setScaleX(1);homeButton.setScaleY(1);});
            homeButton.setOnAction(event -> {
                buzzwordController.goHomeScreen();
            });
            helpPane.setTop(homeButton);
            helpPane.setCenter(helpText);
            helpText.setFill(Color.GOLD);
            helpText.setStyle(" -fx-font-size:30;");

            helpText.setOnScroll(event->{
                helpText.setTranslateY(helpText.getTranslateY()+event.getDeltaY());
            });
            helpScene=new Scene(helpPane);
            helpScene.setOnKeyReleased(event -> {
                if(event.getCode()==KeyCode.H&&event.isControlDown())
                    homeButton.fire();
            });

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void initHandler(){

        helpButton.setOnAction(event -> {
            buzzwordController.goToHelp();
        });
        quitButton.setOnAction(event -> {
            if(logIn){
                yesNoCancelDialog.show("Quit","Do you really want to quit the game? System will save any progress you have made.");
                String s=yesNoCancelDialog.getSelection();
                if(s.equals("Yes")){
                    System.exit(0);
                }
            }
            else {
                System.exit(0);
            }
           });
        startPlayButton.setOnAction(event ->{
            buzzwordController.setMode();
            buzzwordController.renderLevelSelection();
            buzzwordController.goToLevelSelectionScreen();
        });
        newProfileButton.setOnAction(event -> {
            logDialog.show("New Profile");
            String name=logDialog.getUserName();
            String password=logDialog.getPassword();
            if(!(name==null||password==null)){
                if(buzzwordController.createNewProfile(name,password)) {
                    messageDialog.show("Create New Profile", "Success!");
                    updateGui(true);
                }
                else
                    messageDialog.show("Create New Profile", "Failure: This user already exists.");
            }
        });
        logInButton.setOnAction(event -> {
            logDialog.show("Log In");
            String name=logDialog.getUserName();
            String password=logDialog.getPassword();
            if(!(name==null||password==null)) {
                if (buzzwordController.logIn(name, password)) {
                    messageDialog.show("Log In", "Success!");
                    updateGui(true);
                } else
                    messageDialog.show("Log In", "Incorrect username or password");
            }

        });
        profileButton.setOnAction(event -> {
            buzzwordController.setProfile(HOME,false);
        });
    }
    public void updateGui(boolean logIn){
        this.logIn=logIn;
        if(logIn){
            userinformation.setText(buzzwordController.getUserInfo());
            newProfileButton.setVisible(false);
            logInButton.setVisible(false);
            profileButton.setVisible(true);
            modeLabel.setVisible(true);
            modeComboBox.setVisible(true);
            startPlayButton.setVisible(true);
        }
        else {
            modeLabel.setVisible(false);
            modeComboBox.setVisible(false);
            startPlayButton.setVisible(false);
            profileButton.setVisible(false);
            newProfileButton.setVisible(true);
            logInButton.setVisible(true);
        }
    }

    public void initGui(){
        initToolBar();
        homePane=new BorderPane();
        homePane.setLeft(toolBar);
        VBox vBox=new VBox();
        homePane.setRight(vBox);
        helpButton.setClip(new Ellipse(70,65,57,57));
        helpButton.setTranslateY(-5);
        helpButton.setTranslateX(150);
        modeLabel=new Label("Mode:");
        modeLabel.setStyle(" -fx-text-fill: rgb(0, 230, 185);-fx-font: bold italic 50 Arial;");
        modeLabel.setFont(Font.font("Verdana", FontPosture.ITALIC, 20));

        vBox.setTranslateX(-100);
        vBox.setTranslateY(-60);
        quitButton=initButtonView("quit2.png");
        quitButton.setTranslateY(-370);
        quitButton.setTranslateX(285);
        quitButton.setClip(new Ellipse(70,65,57,57));
        vBox.getChildren().addAll(helpButton,modeLabel,modeComboBox,startPlayButton,quitButton);
        modeLabel.setVisible(false);
        modeComboBox.setVisible(false);
        startPlayButton.setVisible(false);
        quitButton.setOnMouseEntered(event -> {quitButton.setScaleX(1.2);quitButton.setScaleY(1.2);});
        quitButton.setOnMouseExited(event -> {quitButton.setScaleX(1);quitButton.setScaleY(1);});
        helpButton.setOnMouseEntered(event -> {helpButton.setScaleX(1.2);helpButton.setScaleY(1.2);});
        helpButton.setOnMouseExited(event -> {helpButton.setScaleX(1);helpButton.setScaleY(1);});
        HBox headBox=new HBox();
        buzzwordLabel=new Label("!!BuzzWord!!");
        buzzwordLabel.setStyle(" -fx-font-size:50; -fx-text-fill: White");
        headBox.getChildren().add(buzzwordLabel);
        buzzwordLabel.setAlignment(Pos.CENTER);
        headBox.setAlignment(Pos.TOP_CENTER);
        homePane.setTop(headBox);
        buzzwordNodes=new GridPane();
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                HBox buzznode=new HBox();
                buzznode.setShape(new Circle(40,Color.WHITE));
                buzznode.setPrefSize(80,80);
                buzznode.setStyle(" -fx-border-radius: 40; -fx-border-width: 3;-fx-border-color: rgb(244, 242, 92);-fx-background-color: White; -fx-font-size: 30pt;");
                if(i==0&&j==0){
                   addText("B",buzznode);
                }
                if(i==0&&j==1){
                    addText("U",buzznode);
                }
                if((i==1&&j==0)||(i==1&&j==1)){
                    addText("Z",buzznode);
                }
                if(i==2&&j==2){
                    addText("W",buzznode);
                }
                if(i==2&&j==3){
                    addText("O",buzznode);
                }
                if(i==3&&j==2){
                    addText("R",buzznode);
                }
                if(i==3&&j==3){
                    addText("D",buzznode);
                }
                buzzwordNodes.add(buzznode,j,i);
            }
        }
        buzzwordNodes.setHgap(50);
        buzzwordNodes.setVgap(50);
        buzzwordNodes.setAlignment(Pos.CENTER);
        homePane.setCenter(buzzwordNodes);
        buzzwordNodes.setTranslateX(0);
        Image saberLily=new Image( getClass().getClassLoader().getResource("images"+File.separator+"Touka.png").toExternalForm());
        BackgroundImage backgroundImage = new BackgroundImage( saberLily, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);
        homePane.setBackground(background);
        //homePane.setStyle("-fx-background-color:  linear-gradient(to right, rgb(205, 105, 222) 2%, rgb(64, 18, 72) 98%);");
    }
    public void addText(String x,HBox buzznode){
        Label b=new Label(x);
        b.setTextFill(Color.BLACK);
        buzznode.getChildren().add(b);
        b.setTranslateX(27);
        b.setTranslateY(12);
    }
    public void initToolBar(){
        startPlayButton=initButtonText("Start Playing");
        startPlayButton.setTranslateY(2);
        newProfileButton=initButtonText("New Profile");
        logInButton=initButtonText("Log In");
        helpButton=initButtonView("help.png");
        profileButton=initButtonText("Username");
        profileButton.setVisible(false);
        profileButton.setTranslateY(-18);
        toolBar=new VBox();
        toolBar.getChildren().addAll(profileButton,newProfileButton,logInButton);
        newProfileButton.setTranslateY(-120);
        logInButton.setTranslateY(-117);
        modeComboBox.getItems().addAll(
                "Places",
                "Science",
                "Animals",
                "Food"
        );
        modeComboBox.setEditable(true);
        modeComboBox.setPrefSize(340,70);
        modeComboBox.setStyle(".combo-box-base  \n" +
                "{\n" +
                "    -fx-skin: \"com.sun.javafx.scene.control.skin.ComboBoxBaseSkin\";\n" +"-fx-font-size:24;"+
                "    -fx-background-color: rgb(100, 48, 0), rgb(100, 48, 0), rgb(100, 48, 0), rgb(100, 48, 0);\n" +
                "-fx-text-fill:rgb(100, 48, 0);"+
                "    -fx-background-radius: 10, 10, 10, 10;\n" +
                "    -fx-background-insets: 0 0 -1 0, 0, 1, 2;\n" +
                "    -fx-padding: 0;\n" +
                "}");

        modeComboBox.setOnAction((Event ev) -> {
            mode = modeComboBox.getSelectionModel().getSelectedItem().toString();
        });
        modeComboBox.setCellFactory(
                new Callback<ListView<String>, ListCell<String>>() {
                    @Override public ListCell<String> call(ListView<String> param) {
                        final ListCell<String> cell = new ListCell<String>() {
                            {
                                super.setPrefWidth(100);
                            }
                            @Override public void updateItem(String item,
                                                             boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item);
                                    setTextFill(Color.rgb(100, 48, 0));
                                }
                                else {
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        toolBar.setAlignment(Pos.CENTER_LEFT);
        toolBar.setStyle("-fx-background-color: transparent;");
        toolBar.setTranslateX(150);
        toolBar.setTranslateY(-60);
    }
    public Button initButtonText(String name){
        Button button=new Button(name);
        button.setPrefSize(340,100);
        button.setStyle("-fx-background-color:rgb(58, 58, 58); -fx-text-fill: rgb(198, 198, 198);-fx-font: bold italic 35 Arial;" +
                " -fx-border-radius: 40; -fx-border-width: 3;-fx-border-color: rgb(244, 242, 92); -fx-background-radius: 40");
        button.setOnMouseEntered(event -> {button.setScaleX(1.2);button.setScaleY(1.05);});
        button.setOnMouseExited(event -> {button.setScaleX(1);button.setScaleY(1);});
        return button;
    }
    public Button initButtonView(String name){
        Button button=new Button();
        URL imgDirURL = getClass().getClassLoader().getResource("images/"+name);
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()))) {
            Image buttonImage=new Image(imgInputStream);
            button.setGraphic(new ImageView(buttonImage));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        button.setOnMouseEntered(event -> {button.setScaleX(1.2);button.setScaleY(1.05);});
        button.setOnMouseExited(event -> {button.setScaleX(1);button.setScaleY(1);});
        return button;
    }
    public Scene getHomeScene(){return homeScene;}
    public String getModeString(){return mode;}
    public Button getProfileButton(){return profileButton;}
    public Scene getHelpScene(){return helpScene;}
    public Scene getProfileScene(){return profileScene;}
    public void setSs(ScreenState s){ss=s;}
    public void setPlaying(boolean b){playing=b;}
    public void setUserinformation(String s){userinformation.setText(s);}
}
