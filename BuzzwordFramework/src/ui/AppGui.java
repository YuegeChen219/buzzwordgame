package ui;

import controller.BuzzwordController;
import javafx.stage.Stage;
import template.BuzzwordTemplate;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by Yuege on 11/8/2016.
 */
public class AppGui {
    protected HomeScreen homeScreen;
    protected LevelSelectionScreen levelSelectionScreen;
    protected GamePlayScreen gamePlayScreen;
    protected Stage primaryStage;
    private double windowHeight;
    private double windowWidth;
    private String appTitle;
    private BuzzwordController buzzwordController;

    public AppGui(Stage primaryStage, double windowHeight, double windowWidth, BuzzwordTemplate bTemplate, String appTitle){
        this.primaryStage=primaryStage;
        this.windowHeight=windowHeight;
        this.windowWidth=windowWidth;
        this.appTitle=appTitle;

        try {
            Class<?>  klass = Class.forName("controller.TheOneAndOnlyController" );
            Constructor<?> constructor                  = klass.getConstructor(BuzzwordTemplate.class);
            buzzwordController = (BuzzwordController) constructor.newInstance(bTemplate);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException|InstantiationException e) {
            e.printStackTrace();
            System.exit(1);
        }

        homeScreen= new HomeScreen(buzzwordController);
        buzzwordController.updateAppGui(this);
        levelSelectionScreen=new LevelSelectionScreen(buzzwordController);
        gamePlayScreen=new GamePlayScreen(buzzwordController);
        buzzwordController.updateAppGui(this);
        initWindow();
    }
    public void initWindow(){
        primaryStage.setTitle(appTitle);
        primaryStage.setScene(homeScreen.getHomeScene());
        primaryStage.show();

    }
    public Stage getPrimaryStage(){return primaryStage;}
    public HomeScreen getHomeScreen(){return homeScreen;}
    public LevelSelectionScreen getLevelSelectionScreen(){return levelSelectionScreen;}
    public GamePlayScreen getGamePlayScreen(){return gamePlayScreen;}
}
