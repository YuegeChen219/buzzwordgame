package ui;

import controller.BuzzwordController;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ui.ScreenState.LEVEL;

/**
 * Created by Yuege on 11/8/2016.
 */
public class LevelSelectionScreen {
    private Scene levelSelectionScene;
    private BorderPane levelPane;
    private FlowPane toolBar;
    private GridPane levelNodes;
    private Button profileButton;
    private Button homeButton;
    private Button quitButton;
    private Label modeLabel;
    private Label buzzwordLabel;
    private BuzzwordController buzzwordController;
    private LogInOffDialog logDialog=LogInOffDialog.getSingleton();
    private MessageDialog messageDialog=MessageDialog.getSingleton();
    private YesNoCancelDialog yesNoCancelDialog=YesNoCancelDialog.getSingleton();
    private final ScreenState ss=LEVEL;
    public Scene getLevelSelectionScene(){return levelSelectionScene;}

    public LevelSelectionScreen(BuzzwordController buzzwordController)  {
        this.buzzwordController=buzzwordController;
        initGui();
        initHandler();
        levelSelectionScene=new Scene(levelPane);
        levelSelectionScene.setOnKeyReleased(event -> {
            if(event.getCode()==KeyCode.H&&event.isControlDown())
                homeButton.fire();
            if(event.getCode()== KeyCode.Q&&event.isControlDown())
                quitButton.fire();
        });
    }

    public void initHandler(){
        profileButton.setOnAction(event -> {
            buzzwordController.setProfile(ss,false);
        });
        quitButton.setOnAction(event -> {
            yesNoCancelDialog.show("Quit","Do you really want to quit the game? System will save any progress you have made.");
            String s=yesNoCancelDialog.getSelection();
            if(s.equals("Yes")){
                System.exit(0);
            }});
        homeButton.setOnAction(event -> {buzzwordController.goHomeScreen();});
    }
    public void updateGui(boolean logIn){}
    public void setModeLabel(String s){modeLabel.setText(s);}
    public void initGui(){
        initToolBar();
        levelPane=new BorderPane();
        levelPane.setLeft(toolBar);
        toolBar.setTranslateY(-60);
        VBox headBox=new VBox();
        buzzwordLabel=new Label("!!BuzzWord!!");
        buzzwordLabel.setStyle(" -fx-font-size:50; -fx-text-fill: White");
        modeLabel=new Label();
        modeLabel.setText(buzzwordController.getMode());
        modeLabel.setStyle(" -fx-font-size:50; -fx-text-fill:  White");
        headBox.getChildren().addAll(buzzwordLabel,modeLabel);
        modeLabel.setTranslateY(100);
        buzzwordLabel.setAlignment(Pos.CENTER);
        headBox.setAlignment(Pos.TOP_CENTER);
        levelPane.setTop(headBox);
        //BackgroundImage backgroundImage = new BackgroundImage( new Image( getClass().getClassLoader().getResource("images"+File.separator+"background.jpg").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        //Background background = new Background(backgroundImage);
        //levelPane.setBackground(background);
        levelNodes=new GridPane();
        for(int i=0;i<2;i++){
            for(int j=0;j<4;j++){
                if(((i*4+4)-(4-j-1))==6)
                    break;
                Button buzznode=new Button((i*4+4)-(4-j-1)+"");
                buzznode.setShape(new Circle(40, Color.WHITE));
                buzznode.setPrefSize(80,80);
                buzznode.setStyle(" -fx-border-radius: 40; -fx-border-width: 3;-fx-border-color: rgb(244, 242, 92);-fx-background-color: White; -fx-font-size: 30pt;");
                buzznode.setTextFill(Color.BLACK);
                buzznode.setDisable(true);
                buzznode.setOnMouseEntered(event -> {buzznode.setScaleX(1.2);buzznode.setScaleY(1.05);});
                buzznode.setOnMouseExited(event -> {buzznode.setScaleX(1);buzznode.setScaleY(1);});
                buzznode.setOnAction(event -> {
                    buzzwordController.setGamePlayLabel(modeLabel.getText(),"Level "+buzznode.getText(),buzznode.getText()
                );
                    buzzwordController.drawLines();
                    buzzwordController.goToGamePlayScreen();
                });
                levelNodes.add(buzznode,j,i);
            }
        }
        levelNodes.setHgap(50);
        levelNodes.setVgap(50);
        levelNodes.getChildren().get(0).setDisable(false);
        levelNodes.setAlignment(Pos.CENTER);
        levelPane.setCenter(levelNodes);
        quitButton = initButtonView("quit2.png");
        quitButton.setClip(new Ellipse(70,65,57,57));
        quitButton.setTranslateX(0);
        quitButton.setTranslateY(-150);
        levelPane.setRight(quitButton);
        levelNodes.setTranslateX(-140);
        levelNodes.setTranslateY(-100);
        Image saberLily=new Image( getClass().getClassLoader().getResource("images"+File.separator+"Touka.png").toExternalForm());
        BackgroundImage backgroundImage = new BackgroundImage( saberLily, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);
        levelPane.setBackground(background);
        //levelPane.setStyle("-fx-background-color:  linear-gradient(to right, rgb(205, 105, 222) 2%, rgb(64, 18, 72) 98%);");
    }

    public void initToolBar(){
        profileButton=initButtonText("Username");
        homeButton=initButtonView("home.png");
        toolBar=new FlowPane();
        toolBar.getChildren().setAll(homeButton,profileButton);
        toolBar.setAlignment(Pos.CENTER_LEFT);
        toolBar.setStyle("-fx-background-color: transparent;");
        toolBar.setTranslateX(30);
        profileButton.setTranslateY(-50);
        homeButton.setTranslateY(-80);
    }
    public Button initButtonView(String name){
        Button button=new Button();
        URL imgDirURL = getClass().getClassLoader().getResource("images"+ File.separator+name);
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()))) {
            Image buttonImage=new Image(imgInputStream);
            button.setGraphic(new ImageView(buttonImage));
            Circle c=new Circle(buttonImage.getWidth()/2+8,buttonImage.getHeight()/2+5,buttonImage.getWidth()/2);
            button.setClip(c);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        button.setOnMouseEntered(event -> {button.setScaleX(1.2);button.setScaleY(1.2);});
        button.setOnMouseExited(event -> {button.setScaleX(1);button.setScaleY(1);});
        return button;
    }
    public Button initButtonText(String name){
        Button button=new Button(name);
        button.setPrefSize(340,100);
        button.setStyle("-fx-background-color:rgb(58, 58, 58); -fx-text-fill: rgb(198, 198, 198);-fx-font: bold italic 35 Arial;" +
                " -fx-border-radius: 40; -fx-border-width: 3;-fx-border-color: rgb(244, 242, 92); -fx-background-radius: 40");
        button.setOnMouseEntered(event -> {button.setScaleX(1.2);button.setScaleY(1.05);});
        button.setOnMouseExited(event -> {button.setScaleX(1);button.setScaleY(1);});
        return button;
    }
    public Button getProfileButton(){return profileButton;}
    public GridPane getLevelNodes(){return levelNodes;}
}

