package ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
/**
 * Created by Yuege on 11/8/2016.
 */
public class YesNoCancelDialog extends Stage {
    static YesNoCancelDialog singleton = null;

    VBox messagePane;
    Scene messageScene;

    Button closeButton;
    Button yesButton;
    Button noButton;
    Label message;
    String selection;
    private YesNoCancelDialog() { }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static YesNoCancelDialog getSingleton() {
        if (singleton == null)
            singleton = new YesNoCancelDialog();
        return singleton;
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        yesButton=new Button("Yes");
        yesButton.setOnAction(event -> {
            selection=yesButton.getText();
            this.close();
        });
        noButton=new Button("No");
        noButton.setOnAction(event -> {
            selection=noButton.getText();
            this.close();
        });
        closeButton=new Button("Cancel");
        closeButton.setOnAction(e -> {
            selection=closeButton.getText();
            this.close();
        });

        // WE'LL PUT EVERYTHING HERE
        messagePane=new VBox();
        messagePane.setAlignment(Pos.CENTER);
        message=new Label("");
        message.setStyle(" -fx-font-size:30; -fx-text-fill: White; ");
        HBox buttonBox=new HBox();
        buttonBox.getChildren().addAll(yesButton,noButton,closeButton);
        buttonBox.setAlignment(Pos.CENTER);
        messagePane.getChildren().addAll(message,buttonBox);

        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(80, 60, 80, 60));
        messagePane.setSpacing(20);
        messagePane.setStyle("-fx-background-color:  linear-gradient(to right, rgb(144, 144, 144) 2%,rgb(83, 0, 0) 49%, rgb(0, 42, 42) 98%);");
        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        messageScene.setOnKeyReleased((KeyEvent e)->{
            if(e.getCode()== KeyCode.ENTER){
                yesButton.fire();
            }
        });
        this.setScene(messageScene);
    }
    public String getSelection(){
        return selection;
    }
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window.
     */
    public void show(String title,String message) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);
        this.message.setText(message);
        // SET THE MESSAGE TO DISPLAY TO THE USER

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }
}
