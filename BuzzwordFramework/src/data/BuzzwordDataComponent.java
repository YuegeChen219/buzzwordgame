package data;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Yuege on 11/11/2016.
 */
public class BuzzwordDataComponent {

    private final SimpleStringProperty word;
    private final SimpleStringProperty score;

    public BuzzwordDataComponent(String word, String score) {
        this.word = new SimpleStringProperty(word);
        this.score = new SimpleStringProperty(score);
    }

    public String getWord() {
        return word.get();
    }

    public void setWord(String fword) {
        word.set(fword);
    }

    public String getScore() {
        return score.get();
    }

    public void setScore(String fscore) {
        score.set(fscore);
    }
}
